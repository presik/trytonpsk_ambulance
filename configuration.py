# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, ModelSingleton
from trytond.pyson import Eval, If, Bool, Id


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Configuration'
    __name__ = 'ambulance.configuration'

    bitacora_sequence = fields.Many2One('ir.sequence',
        'Bitacora Sequence', domain=[
            ('company', 'in', [Eval('context', {}).get('company', 0), None]),
            ('sequence_type', '=',
                Id('ambulance', 'sequence_type_ambulance'))
        ], required=True)
    enablement_code = fields.Char('Enablement Code')
