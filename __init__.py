    # This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import service_ambulance


def register():
    Pool.register(
        configuration.Configuration,
        service_ambulance.ServiceAmbulance,
        module='ambulance', type_='model')
    Pool.register(
        service_ambulance.ServiceAmbulanceBitacoraReport,
        service_ambulance.ServiceAmbulanceFurtranReport,
        module='ambulance', type_='report')
